#include "tcpip.h"
#include <arpa/inet.h>
#include <iostream>
#include <netinet/in.h>
#include <pthread.h>
#include <stdexcept>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

struct {
    string host;
    unsigned short port;
    int concurrent;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":c:");
        switch (c) {
        case -1:
            goto out;
        case 'c':
            options.concurrent = stoi(optarg);
            break;
        default:
            throw invalid_argument(string{(char)c});
        }
    }
out:
    if (optind + 2 != argc)
        throw invalid_argument("");
    options.host = argv[optind++];
    options.port = stoi(argv[optind++]);
}

int dial_tcp(const string &host, unsigned short port)
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");
    struct sockaddr_in addr = {};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = nametoaddr(host);
    addr.sin_port = htons(port);
    if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("connect");
    return sockfd;
}

static pthread_mutex_t mylock = PTHREAD_MUTEX_INITIALIZER;

void print_msg(const string &msg)
{
    pthread_mutex_lock(&mylock);
    cout << msg << endl;
    pthread_mutex_unlock(&mylock);
}

void *run(void *arg)
{
    try {
        int connfd = dial_tcp(options.host, options.port);
        char c;
        if (recv_timeout(connfd, &c, 1, 0, 10000) < 0)
            throw unix_error("recv_timeout");
        print_msg("dial_tcp: success");
    } catch (const exception &e) {
        print_msg(e.what());
    }
    return NULL;
}

void backlogtest()
{
    if (options.concurrent > 0) {
        for (int i = 0; i < options.concurrent; ++i) {
            pthread_t tid;
            pthread_create(&tid, NULL, run, NULL);
        }
    } else {
        for (int i = 1;; ++i) {
            dial_tcp(options.host, options.port);
            cout << 1 << endl;
        }
    }
    for (;;)
        pause();
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        backlogtest();
    } catch (const invalid_argument &e) {
        cout << "usage: backlogtest [-c concurrent] host port" << endl;
    } catch (const exception &e) {
        cout << e.what() << endl;
    }
}
