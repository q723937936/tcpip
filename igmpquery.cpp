#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/igmp.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

uint16_t in_cksum(uint16_t *addr, int len)
{
    int nleft = len;
    uint32_t sum = 0;
    uint16_t *w = addr;
    uint16_t answer = 0;

    /*
     * Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (nleft > 1) {
        sum += *w++;
        nleft -= 2;
    }

    /* 4mop up an odd byte, if necessary */
    if (nleft == 1) {
        *(unsigned char *)(&answer) = *(unsigned char *)w;
        sum += answer;
    }

    /* 4add back carry outs from top 16 bits to low 16 bits */
    sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
    sum += (sum >> 16);                 /* add carry */
    answer = ~sum;                      /* truncate to 16 bits */
    return (answer);
}

void igmpquery()
{
    int sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_IGMP);
    if (sockfd < 0)
        throw unix_error("socket");

    struct igmp igmp = {};
    igmp.igmp_type = IGMP_MEMBERSHIP_QUERY;
    igmp.igmp_cksum = in_cksum((unsigned short *)&igmp, sizeof(igmp));

    struct sockaddr_in addr = {};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr("224.0.0.1");

    if (sendto(sockfd, &igmp, sizeof(igmp), 0, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("sendto");
}

int main(int argc, char *argv[])
{
    try {
        igmpquery();
    } catch (const runtime_error &e) {
        cout << e.what() << endl;
    }
}
