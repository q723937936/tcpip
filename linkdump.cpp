#include <arpa/inet.h>
#include <boost/algorithm/hex.hpp>
#include <iostream>
#include <iterator>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <strings.h>
#include <sys/socket.h>

using namespace std;

void linkdump(const unsigned char *ptr, size_t len, const struct sockaddr_ll *addr)
{
    switch (addr->sll_pkttype) {
    case PACKET_HOST:
        cout << "host";
        break;
    case PACKET_BROADCAST:
        cout << "broadcast";
        break;
    case PACKET_MULTICAST:
        cout << "multicast";
        break;
    case PACKET_OTHERHOST:
        cout << "otherhost";
        break;
    case PACKET_OUTGOING:
        cout << "outgoing";
        break;
    default:
        cout << "unknown";
        break;
    }
    cout << ":" << len;
    string s;
    // 只打印以太网帧的前60个字节
    boost::algorithm::hex(ptr, ptr + 60, back_inserter(s));
    cout << ":" << s << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("usage: linkdump interface\n");
        exit(1);
    }
    unsigned int ifindex = if_nametoindex(argv[1]);
    if (ifindex == 0) {
        perror("if_nametoindex");
        exit(1);
    }
    int sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sockfd < 0) {
        perror("socket");
        exit(1);
    }
    struct sockaddr_ll addr;
    bzero(&addr, sizeof(addr));
    addr.sll_family = AF_PACKET;
    addr.sll_protocol = htons(ETH_P_ALL);
    addr.sll_ifindex = (int)ifindex;
    if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("bind");
        exit(1);
    }
    unsigned char buf[1024];
    for (;;) {
        struct sockaddr_ll src_addr;
        bzero(&src_addr, sizeof(src_addr));
        socklen_t src_addr_len = sizeof(src_addr);
        ssize_t nrecv = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *)&src_addr, &src_addr_len);
        if (nrecv < 0) {
            perror("recvfrom");
            exit(1);
        }
        linkdump(buf, nrecv, &src_addr);
    }
}
