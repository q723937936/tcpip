#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("usage: mtu interface\n");
        exit(1);
    }
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        perror("socket");
        exit(1);
    }
    struct ifreq req;
    bzero(&req, sizeof(req));
    strcpy(req.ifr_name, argv[1]);
    if (ioctl(sockfd, SIOCGIFMTU, &req) < 0) {
        perror("ioctl");
        exit(1);
    }
    printf("%ld\n", (long)req.ifr_mtu);
}
