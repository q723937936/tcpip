#include "tcpip.h"
#include <arpa/inet.h>
#include <iostream>
#include <linux/if_packet.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sstream>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

using namespace std;

struct {
    string iface;
    bool hexdump;
    bool numeric;
    string host;
    unsigned short port;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":i:xn");
        switch (c) {
        case -1:
            goto out;
        case 'i':
            options.iface = optarg;
            break;
        case 'x':
            options.hexdump = true;
            break;
        case 'n':
            options.numeric = true;
            break;
        default:
            throw invalid_argument(string{(char)c});
        }
    }
out:
    while (optind < argc) {
        string opt = argv[optind++];
        if (opt == "host")
            options.host = inet_ntoa({nametoaddr(argv[optind])});
        else if (opt == "port")
            options.port = stoi(argv[optind]);
        else
            throw invalid_argument(opt);
        ++optind;
    }
}

struct __attribute__((__packed__)) mytcp {
    struct ethhdr eth_hdr;
    struct iphdr ip_hdr;
    struct tcphdr tcp_hdr;
    unsigned char tcp_options[1];
};

struct ipaddr {
    in_addr_t in_addr;
};

ostream &operator<<(ostream &out, const ipaddr &ip)
{
    if (options.numeric)
        out << inet_ntoa({ip.in_addr});
    else
        out << addrtoname(ip.in_addr);
    return out;
}

void hexdump(const unsigned char *ptr, size_t len)
{
    stringstream ss;
    char buf[16];

    for (size_t i = 0; i < len; i += 2) {
        if (i % 16) {
            ss << ' ';
        } else {
            sprintf(buf, "\t0x%04x:  ", (unsigned int)i);
            ss << '\n'
               << buf;
        }
        sprintf(buf, "%02x%02x", ptr[i], ptr[i + 1]);
        ss << buf;
    }
    string s = ss.str();
    if (!s.empty())
        cout << &ss.str().c_str()[1];
    cout << endl;
}

string cksum(unsigned short cs)
{
    char buf[8];
    sprintf(buf, "%04x", (unsigned int)ntohs(cs));
    return buf;
}

struct tcp_flags {
    struct tcphdr tcp_hdr;
};

struct tcp_options {
    unsigned char *options_ptr;
    int options_len;
};

ostream &operator<<(ostream &out, const tcp_flags &rhs)
{
    out << "[";
    if (rhs.tcp_hdr.fin) out << "F";
    if (rhs.tcp_hdr.syn) out << "S";
    if (rhs.tcp_hdr.rst) out << "R";
    if (rhs.tcp_hdr.psh) out << "P";
    if (rhs.tcp_hdr.ack) out << "A";
    if (rhs.tcp_hdr.urg) out << "U";
    out << "]";
    return out;
}

ostream &operator<<(ostream &out, const tcp_options &rhs)
{
    unsigned char *ptr = rhs.options_ptr,
                  *endptr = ptr + rhs.options_len;
    string sep = "";

    out << "[";
    while (ptr < endptr) {
        switch (*ptr) {
        case TCPOPT_EOL:
            goto done;
        case TCPOPT_NOP:
            ++ptr;
            continue;
        case TCPOPT_MAXSEG: {
            unsigned short mss;
            memcpy(&mss, ptr + 2, sizeof(mss));
            mss = ntohs(mss);
            out << sep << "mss " << mss;
            break;
        }
        case TCPOPT_WINDOW:
            out << sep << "ws " << (int)(*(ptr + 2));
            break;
        case TCPOPT_TIMESTAMP: {
            unsigned int val, ecr;
            memcpy(&val, ptr + 2, sizeof(val));
            memcpy(&ecr, ptr + 6, sizeof(ecr));
            val = ntohl(val);
            ecr = ntohl(ecr);
            out << sep << "ts " << val << " " << ecr;
            break;
        }
        default:
            break;
        }
        sep = ",";

        ++ptr;
        ptr += *ptr;
        --ptr;
    }
done:
    out << "]";
    return out;
}

inline int tcp_data_len(const mytcp *tcp)
{
    return ntohs(tcp->ip_hdr.tot_len) - (tcp->ip_hdr.ihl << 2) - (tcp->tcp_hdr.doff << 2);
}

inline int to_us(const struct timeval *tv)
{
    return (tv->tv_sec * 1000000 + tv->tv_usec);
}

string recvtime(const struct timeval *tv,
                const struct timeval *start_tv,
                const struct timeval *prev_tv)
{
    int tv_us, start_tv_us, prev_tv_us;
    int delta1_us, delta2_us;
    char buf[100];

    tv_us = to_us(tv);
    start_tv_us = to_us(start_tv);
    prev_tv_us = to_us(prev_tv);

    delta1_us = tv_us - start_tv_us;
    delta2_us = tv_us - prev_tv_us;

    sprintf(buf, "%2d.%03d%03d (%2d.%03d%03d)",
            delta1_us / 1000000, (delta1_us / 1000) % 1000, delta1_us % 1000,
            delta2_us / 1000000, (delta2_us / 1000) % 1000, delta2_us % 1000);
    return buf;
}

static struct timeval start_tv, prev_tv;

void tcpdump(const unsigned char *ptr, size_t len, const struct timeval *tv)
{
    mytcp *tcp = (mytcp *)ptr;

    if (tcp->eth_hdr.h_proto != htons(ETH_P_IP))
        return;
    if (tcp->ip_hdr.protocol != IPPROTO_TCP)
        return;

    string saddr = inet_ntoa({tcp->ip_hdr.saddr});
    string daddr = inet_ntoa({tcp->ip_hdr.daddr});
    unsigned short sport = ntohs(tcp->tcp_hdr.source);
    unsigned short dport = ntohs(tcp->tcp_hdr.dest);

    if (options.host.empty())
        goto andport;
    if (options.host == saddr || options.host == daddr)
        goto andport;
    return;

andport:
    if (options.port == 0)
        goto dodump;
    if (options.port == sport || options.port == dport)
        goto dodump;
    return;

dodump:
    if (start_tv.tv_sec == 0)
        start_tv = prev_tv = *tv;

    cout << recvtime(tv, &start_tv, &prev_tv)
         << " IP " << ipaddr{tcp->ip_hdr.saddr} << "." << sport
         << " > " << ipaddr{tcp->ip_hdr.daddr} << "." << dport
         << ": flags " << tcp_flags{tcp->tcp_hdr}
         << ", seq " << ntohl(tcp->tcp_hdr.seq);
    if (tcp->tcp_hdr.ack)
        cout << ", ack " << ntohl(tcp->tcp_hdr.ack_seq);
    cout << ", win " << ntohs(tcp->tcp_hdr.window);
    if (tcp->tcp_hdr.urg)
        cout << ", urg " << ntohs(tcp->tcp_hdr.urg_ptr);
    cout << ", options " << tcp_options{tcp->tcp_options, (tcp->tcp_hdr.doff - 5) * 4}
         << ", length " << tcp_data_len(tcp)
         << endl;

    prev_tv = *tv;

    if (options.hexdump)
        hexdump(ptr, len);
}

void do_listen()
{
    int sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sockfd < 0)
        throw unix_error("socket");

    if (!options.iface.empty()) {
        unsigned int iface_idx = if_nametoindex(options.iface.c_str());
        if (iface_idx == 0)
            throw unix_error("if_nametoindex");
        struct sockaddr_ll addr;
        bzero(&addr, sizeof(addr));
        addr.sll_family = AF_PACKET;
        addr.sll_protocol = htons(ETH_P_ALL);
        addr.sll_ifindex = iface_idx;
        if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            throw unix_error("bind");
    }

    for (;;) {
        unsigned char buf[1024];
        ssize_t nrecv = recvfrom(sockfd, buf, sizeof(buf), 0, NULL, NULL);
        if (nrecv < 0)
            throw unix_error("recvfrom");
        struct timeval tv;
        if (ioctl(sockfd, SIOCGSTAMP, &tv) < 0)
            throw unix_error("ioctl");
        tcpdump(buf, nrecv, &tv);
    }
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        do_listen();
    } catch (const runtime_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: " << argv[0] << " [-i interface] [-x] [-n] [host ip] [port number]" << endl;
    }
}
