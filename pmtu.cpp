#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

int main(int argc, char *argv[])
{
    if (argc != 4) {
        cout << "usage: pmtu host port size" << endl;
        return 1;
    }

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    addr.sin_port = htons(stoi(argv[2]));

    int optval = IP_PMTUDISC_PROBE;
    if (setsockopt(sockfd, IPPROTO_IP, IP_MTU_DISCOVER, &optval, sizeof(optval)) < 0)
        throw unix_error("setsockopt");

    int size = stoi(argv[3]);
    string s(size, 'x');
    if (sendto(sockfd, s.data(), size, 0, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("sendto");
}
