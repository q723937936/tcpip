#include <algorithm>
#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

struct {
    string localhost, remotehost;
    unsigned short localport, remoteport;
    bool reuseaddr;
    int delay;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":Ah:p:d:");
        switch (c) {
        case -1:
            goto out;
        case 'A':
            options.reuseaddr = true;
            break;
        case 'h':
            options.localhost = optarg;
            break;
        case 'p':
            options.localport = stoi(optarg);
            break;
        case 'd':
            options.delay = stoi(optarg);
            break;
        default:
            throw invalid_argument(string{(char)c});
        }
    }
out:
    if (optind + 2 != argc)
        throw invalid_argument("");
    options.remotehost = argv[optind++];
    options.remoteport = stoi(argv[optind++]);
}

int myselect(initializer_list<int> fds)
{
    fd_set readfds;
    FD_ZERO(&readfds);
    for (int fd : fds)
        FD_SET(fd, &readfds);
    if (select(std::max(fds) + 1, &readfds, NULL, NULL, NULL) < 0)
        throw unix_error("select");
    for (int fd : fds) {
        if (FD_ISSET(fd, &readfds))
            return fd;
    }
    throw unix_error("select");
}

void simpletcpclient()
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");

    if (options.reuseaddr) {
        int optval = 1;
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0)
            throw unix_error("setsockopt");
    }

    if (!options.localhost.empty() || options.localport > 0) {
        struct sockaddr_in addr = {};
        addr.sin_family = AF_INET;
        if (options.localhost.empty())
            addr.sin_addr.s_addr = htonl(INADDR_ANY);
        else
            addr.sin_addr.s_addr = inet_addr(options.localhost.c_str());
        addr.sin_port = htons(options.localport);
        if (::bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            throw unix_error("bind");
    }

    if (options.delay > 0)
        sleep(options.delay);

    struct sockaddr_in addr = {};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(options.remotehost.c_str());
    addr.sin_port = htons(options.remoteport);
    if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("connect");

    for (;;) {
        int fd = myselect({0, sockfd});
        if (fd == 0) {
            string s;
            if (!getline(cin, s))
                break;
            ssize_t nsend = send(sockfd, s.c_str(), s.size(), 0);
            if (nsend < 0)
                throw unix_error("send");
        } else {
            char buf[1024];
            ssize_t nrecv = recv(sockfd, buf, sizeof(buf) - 1, 0);
            if (nrecv < 0)
                throw unix_error("recv");
            if (nrecv == 0)
                break;
            buf[nrecv] = '\0';
            cout << buf << endl;
        }
    }
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        simpletcpclient();
    } catch (const unix_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: simpletcpclient [-A] [-h localhost] [-p localport] [-d delay] host port" << endl;
    }
}
