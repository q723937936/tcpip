#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

struct {
    unsigned short port;
    bool reuseaddr;
    int delay;
    int backlog = 10;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":Ad:q:");
        switch (c) {
        case -1:
            goto out;
        case 'A':
            options.reuseaddr = true;
            break;
        case 'd':
            options.delay = stoi(optarg);
            break;
        case 'q':
            options.backlog = stoi(optarg);
            break;
        default:
            throw invalid_argument(string{(char)c});
        }
    }
out:
    if (optind + 1 != argc)
        throw invalid_argument("");
    options.port = stoi(argv[optind++]);
}

ostream &operator<<(ostream &out, const struct sockaddr_in &rhs)
{
    out << inet_ntoa(rhs.sin_addr) << ":" << ntohs(rhs.sin_port);
    return out;
}

void simpletcpserver()
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");

    if (options.reuseaddr) {
        int optval = 1;
        if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0)
            throw unix_error("setsockopt");
    }

    struct sockaddr_in addr = {};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(options.port);
    if (::bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("bind");

    if (listen(sockfd, options.backlog) < 0)
        throw unix_error("listen");

    if (options.delay > 0)
        sleep(options.delay);

    for (;;) {
        struct sockaddr_in remote_addr;
        socklen_t remote_addr_len = sizeof(remote_addr);
        int connfd = accept(sockfd, (struct sockaddr *)&remote_addr, &remote_addr_len);
        if (connfd < 0)
            throw unix_error("accept");
        cout << "connection from " << remote_addr << endl;
    }
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        simpletcpserver();
    } catch (const unix_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: simpletcpserver [-A] [-d delay] [-q backlog] port" << endl;
    }
}
