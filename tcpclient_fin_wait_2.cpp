#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

struct {
    string remotehost;
    unsigned short remoteport;
    bool shutdown;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":s");
        switch (c) {
        case -1:
            goto out;
        case 's':
            options.shutdown = true;
            break;
        default:
            throw invalid_argument(string{(char)c});
        }
    }
out:
    if (optind + 2 != argc)
        throw invalid_argument("");
    options.remotehost = argv[optind++];
    options.remoteport = stoi(argv[optind++]);
}

void tcpclient_fin_wait_2()
{
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");

    struct sockaddr_in addr = {};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(options.remotehost.c_str());
    addr.sin_port = htons(options.remoteport);
    if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("connect");

    if (options.shutdown)
        shutdown(sockfd, SHUT_WR);
    else
        close(sockfd);

    for (;;)
        pause();
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        tcpclient_fin_wait_2();
    } catch (const unix_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: tcpclient_fin_wait_2 [-s] host port"
             << "\n\t-s\tuse shutdown" << endl;
    }
}
