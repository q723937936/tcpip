#include "tcpip.h"
#include <sys/select.h>

int connect_timeout(int sockfd, struct sockaddr *addr, socklen_t len, int timeout_ms)
{
    int oldflags, nready, error;
    socklen_t optlen;
    fd_set rset, wset;
    struct timeval tval;

    oldflags = set_nonblock(sockfd);
    if (oldflags < 0)
        return -1;
    if (connect(sockfd, addr, len) == 0) {
    ok:
        fcntl(sockfd, F_SETFL, oldflags);
        return 0;
    }
    if (errno != EINPROGRESS)
        return -1;
    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    wset = rset;
    tval.tv_sec = timeout_ms / 1000;
    tval.tv_usec = timeout_ms % 1000 * 1000;
    nready = select(sockfd + 1, &rset, &wset, NULL, timeout_ms ? &tval : NULL);
    switch (nready) {
    case 0: // timeout
        errno = ETIMEDOUT;
    case -1:
        return -1;
    default:
        break;
    }
    optlen = sizeof(error);
    if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &optlen) < 0)
        return -1;
    if (error) {
        errno = error;
        return -1;
    }
    goto ok;
}

ssize_t recv_timeout(int sockfd, void *buf, size_t len, int flags, int timeout_ms)
{
    fd_set rset;
    struct timeval tval;
    int nready, error;
    socklen_t optlen;

    FD_ZERO(&rset);
    FD_SET(sockfd, &rset);
    tval.tv_sec = timeout_ms / 1000;
    tval.tv_usec = timeout_ms % 1000 * 1000;
    nready = select(sockfd + 1, &rset, NULL, NULL, timeout_ms ? &tval : NULL);
    switch (nready) {
    case 0: // timeout
        errno = ETIMEDOUT;
    case -1:
        return -1;
    default:
        break;
    }
    optlen = sizeof(error);
    if (getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &optlen) < 0)
        return -1;
    if (error) {
        errno = error;
        return -1;
    }
    return recv(sockfd, buf, len, flags);
}
