#ifndef TCPIP_H
#define TCPIP_H

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>

struct unix_error : std::runtime_error {
    explicit unix_error(const std::string &s) : std::runtime_error(s + ": " + strerror(errno)) {}
};

struct host_error : std::runtime_error {
    explicit host_error(const std::string &s) : std::runtime_error(s + ": " + hstrerror(h_errno)) {}
};

inline in_addr_t nametoaddr(const std::string &name)
{
    struct hostent *ent;
    in_addr_t addr;

    ent = gethostbyname(name.c_str());
    if (!ent)
        throw host_error("gethostbyname");
    memcpy(&addr, ent->h_addr, sizeof(addr));
    return addr;
}

inline std::string addrtoname(in_addr_t addr)
{
    struct hostent *ent;

    ent = gethostbyaddr(&addr, sizeof(addr), AF_INET);
    if (!ent)
        return inet_ntoa({addr});
    return ent->h_name;
}

inline int set_nonblock(int fd)
{
    int flags;
    flags = fcntl(fd, F_GETFL);
    if (flags < 0)
        return -1;
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) < 0)
        return -1;
    return flags;
}

int connect_timeout(int sockfd, struct sockaddr *addr, socklen_t len, int timeout_ms);
ssize_t recv_timeout(int sockfd, void *buf, size_t len, int flags, int timeout_ms);

#endif
