#include <algorithm>
#include <arpa/inet.h>
#include <errno.h>
#include <initializer_list>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

struct host_error : runtime_error {
    explicit host_error(const string &s) : runtime_error(s + ": " + hstrerror(h_errno)) {}
};

struct {
    bool connect_flag;
    bool broadcast_flag;
    string host;
    int port;
} options;

int myselect(initializer_list<int> fds)
{
    fd_set readfds;
    FD_ZERO(&readfds);
    for (int fd : fds)
        FD_SET(fd, &readfds);
    if (select(std::max(fds) + 1, &readfds, NULL, NULL, NULL) < 0)
        throw unix_error("select");
    for (int fd : fds) {
        if (FD_ISSET(fd, &readfds))
            return fd;
    }
    throw unix_error("select");
}

void udpcli()
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");

    if (options.broadcast_flag) {
        int optval = 1;
        if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval)) < 0)
            throw unix_error("setsockopt");
    }

    struct hostent *ent = gethostbyname(options.host.c_str());
    if (!ent)
        throw host_error("gethostbyname");

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    memcpy(&addr.sin_addr, ent->h_addr, sizeof(struct in_addr));
    addr.sin_port = htons((unsigned short)options.port);

    if (options.connect_flag) {
        if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            throw unix_error("connect");
    }

    for (;;) {
        int fd = myselect({0, sockfd});
        if (fd == 0) {
            string s;
            if (getline(cin, s)) {
                ssize_t nsend = sendto(sockfd, s.c_str(), s.size(), 0, (struct sockaddr *)&addr, sizeof(addr));
                if (nsend < 0)
                    throw unix_error("sendto");
            }
        } else {
            char buf[2048] = {};
            struct sockaddr_in reply_addr = {};
            socklen_t reply_addr_len = sizeof(reply_addr);
            if (recvfrom(fd, buf, sizeof(buf), 0, (struct sockaddr *)&reply_addr, &reply_addr_len) < 0)
                throw unix_error("recvfrom");
            cout << "from " << inet_ntoa(reply_addr.sin_addr) << ": " << buf << endl;
        }
    }
}

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":bc");
        switch (c) {
        case -1:
            goto out;
        case 'b':
            options.broadcast_flag = true;
            break;
        case 'c':
            options.connect_flag = true;
            break;
        default:
            throw invalid_argument("");
        }
    }
out:
    if (optind + 2 != argc)
        throw invalid_argument("");
    options.host = argv[optind++];
    options.port = stoi(argv[optind++]);
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        udpcli();
    } catch (const runtime_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: udpcli [-b] [-c] host port" << endl;
    }
}
