#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

#define throw_unix_error(fn) throw runtime_error(string(fn) + ": " + strerror(errno))

void udpclittl(const string &host, int port, int ttl)
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        throw_unix_error("socket");

    if (setsockopt(sockfd, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl)) < 0)
        throw_unix_error("setsockopt");

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(host.c_str());
    addr.sin_port = htons(port);

    ssize_t nsend = sendto(sockfd, "", 1, 0, (struct sockaddr *)&addr, sizeof(addr));
    if (nsend < 0)
        throw_unix_error("sendto");
}

int main(int argc, char *argv[])
{
    if (argc != 4) {
        cout << "usage: udpclittl host port ttl" << endl;
        return 0;
    }
    try {
        udpclittl(argv[1], stoi(argv[2]), stoi(argv[3]));
    } catch (const runtime_error &e) {
        cout << e.what() << endl;
    }
}
