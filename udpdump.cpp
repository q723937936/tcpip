#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <linux/if_packet.h>
#include <net/if.h>
#include <netinet/if_ether.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <sstream>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

#define throw_unix_error(fn) throw runtime_error(string(fn) + ": " + strerror(errno))

struct {
    string iface;
    bool hexdump = false;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":i:x");
        switch (c) {
        case -1:
            return;
        case 'i':
            options.iface = optarg;
            break;
        case 'x':
            options.hexdump = true;
            break;
        default:
            throw invalid_argument(string{(char)c});
        }
    }
}

struct __attribute__((__packed__)) myudp {
    struct ethhdr eth_hdr;
    struct iphdr ip_hdr;
    struct udphdr udp_hdr;
    unsigned char udp_data[1];
};

struct ipaddr {
    in_addr_t in_addr;
};

ostream &operator<<(ostream &out, const ipaddr &ip)
{
    struct in_addr in;
    in.s_addr = ip.in_addr;
    out << inet_ntoa(in);
    return out;
}

void hexdump(const unsigned char *ptr, size_t len)
{
    stringstream ss;
    char buf[16];

    for (size_t i = 0; i < len; i += 2) {
        if (i % 16) {
            ss << ' ';
        } else {
            sprintf(buf, "\t0x%04x:  ", (unsigned int)i);
            ss << '\n'
               << buf;
        }
        sprintf(buf, "%02x%02x", ptr[i], ptr[i + 1]);
        ss << buf;
    }
    string s = ss.str();
    if (!s.empty())
        cout << &ss.str().c_str()[1];
    cout << endl;
}

string cksum(unsigned short cs)
{
    char buf[8];
    sprintf(buf, "%04x", (unsigned int)ntohs(cs));
    return buf;
}

void udpdump(const unsigned char *ptr, size_t len)
{
    myudp *udp = (myudp *)ptr;

    if (udp->eth_hdr.h_proto != htons(ETH_P_IP))
        return;
    if (udp->ip_hdr.protocol != IPPROTO_UDP)
        return;

    cout << "IP " << ipaddr{udp->ip_hdr.saddr} << "." << ntohs(udp->udp_hdr.source)
         << " > " << ipaddr{udp->ip_hdr.daddr} << "." << ntohs(udp->udp_hdr.dest)
         << ": UDP, length " << (ntohs(udp->udp_hdr.len) - 8)
         << " (UDP cksum=" << cksum(udp->udp_hdr.check) << ")"
         << endl;

    if (options.hexdump)
        hexdump(ptr, len);
}

void do_listen()
{
    int sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (sockfd < 0)
        throw_unix_error("socket");

    if (!options.iface.empty()) {
        unsigned int iface_idx = if_nametoindex(options.iface.c_str());
        if (iface_idx == 0)
            throw_unix_error("if_nametoindex");
        struct sockaddr_ll addr;
        bzero(&addr, sizeof(addr));
        addr.sll_family = AF_PACKET;
        addr.sll_protocol = htons(ETH_P_ALL);
        addr.sll_ifindex = iface_idx;
        if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
            throw_unix_error("bind");
    }

    for (;;) {
        unsigned char buf[1024];
        ssize_t nrecv = recvfrom(sockfd, buf, sizeof(buf), 0, NULL, NULL);
        if (nrecv < 0)
            throw_unix_error("recvfrom");
        udpdump(buf, nrecv);
    }
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        do_listen();
    } catch (const runtime_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: " << argv[0] << " [-i interface] [-x]" << endl;
    }
}
