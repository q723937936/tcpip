#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <stdexcept>
#include <string.h>
#include <string>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

struct unix_error : runtime_error {
    explicit unix_error(const string &s) : runtime_error(s + ": " + strerror(errno)) {}
};

struct {
    string mcast_addr;
    unsigned short port;
} options;

void parse_options(int argc, char *argv[])
{
    for (;;) {
        int c = getopt(argc, argv, ":m:");
        switch (c) {
        case -1:
            goto out;
        case 'm':
            options.mcast_addr = optarg;
            break;
        default:
            throw invalid_argument("");
        }
    }
out:
    if (optind + 1 != argc)
        throw invalid_argument("");
    options.port = stoi(argv[optind++]);
}

void udpecho()
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        throw unix_error("socket");

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(options.port);

    if (bind(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        throw unix_error("bind");

    if (!options.mcast_addr.empty()) {
        struct ip_mreq mreq = {};
        mreq.imr_multiaddr.s_addr = inet_addr(options.mcast_addr.c_str());
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);
        if (setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0)
            throw unix_error("setsockopt");
    }

    for (;;) {
        char buf[2048] = {};
        socklen_t addr_len = sizeof(addr);
        ssize_t nrecv = recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *)&addr, &addr_len);
        if (nrecv < 0)
            throw unix_error("recvfrom");

        ssize_t nsend = sendto(sockfd, buf, nrecv, 0, (struct sockaddr *)&addr, sizeof(addr));
        if (nsend < 0)
            throw unix_error("sendto");
    }
}

int main(int argc, char *argv[])
{
    try {
        parse_options(argc, argv);
        udpecho();
    } catch (const unix_error &e) {
        cout << e.what() << endl;
    } catch (const invalid_argument &e) {
        cout << "usage: udpecho [-m mcast_addr] port" << endl;
    }
}
